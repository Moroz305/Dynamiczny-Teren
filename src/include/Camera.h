#ifndef DYNAMICZNYTEREN_CAMERA_H
#define DYNAMICZNYTEREN_CAMERA_H

#include <glm/glm.hpp>

enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

class Camera {
public:
    inline static Camera* camera;

    glm::vec3 position = glm::vec3(0);
    glm::vec3 front = glm::vec3(0.0f, 0.0f, -1.0f);
    glm::vec3 up = glm::vec3(0);
    glm::vec3 right = glm::vec3(0);
    glm::vec3 worldUp = glm::vec3(0.0f, 1.0f, 0.0f);
    float yaw = -90.0f;
    float pitch = -45.0f;
    float movementSpeed = 15.0f;
    float mouseSensitivity = 0.01f;
    float zoom = 45.0f;

public:
    ~Camera();

    static Camera* GetInstance();

    [[nodiscard]] glm::mat4 GetViewMatrix() const;
    void ProcessKeyboard(Camera_Movement direction, float deltaTime);
    void ProcessMouseMovement(float xoffset, float yoffset);
    void ProcessZoom(float yoffset);
    void ProcessMouseScroll(float yoffset);

private:
    Camera();

    void UpdateCameraVectors();
};

#endif //DYNAMICZNYTEREN_CAMERA_H