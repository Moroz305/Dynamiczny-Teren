#ifndef DYNAMICZNYTEREN_MESH_H
#define DYNAMICZNYTEREN_MESH_H

#include <glm/glm.hpp>
#include <string>
#include <vector>

class Shader;

struct Vertex {
    // position
    glm::vec3 position;
    // normal
    glm::vec3 normal;
    // texCoords
    glm::vec2 texCoords;
    // tangent
    glm::vec3 tangent;
    // bitangent
    glm::vec3 bitangent;
};

struct Texture {
    unsigned int id;
    std::string type;
    std::string path;
};

class Mesh {
public:
    // mesh Data
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;
    unsigned int vao = 0;

    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures);
    ~Mesh();

    void Draw(Shader* shader, unsigned int instancesAmount, bool isItVegetation);

private:
    // render data
    unsigned int vbo = 0;
    unsigned int ebo = 0;

    void SetupMesh();
};

#endif //DYNAMICZNYTEREN_MESH_H