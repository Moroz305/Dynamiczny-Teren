#ifndef DYNAMICZNYTEREN_TERRAIN_H
#define DYNAMICZNYTEREN_TERRAIN_H

#include <vector>
#include "Shader.h"
#include "Mesh.h"

class Terrain {
public:
    inline static Terrain* terrain;

    // Heightmap
    float frequency = 8.0;
    int octaves = 2;
    unsigned int seed = 0;
    unsigned int heightMapID = 0;
    short heightmapLength = 1024;
    int mountainOctaves = 16;

    // ForestMap
    float probability = 0.5;
    int iterations = 5;
    int activeNeighboursRequired[5]{3, 4, 4, 4, 4};
    unsigned int forestMapID = 0;
    short forestMapLength = 64;

    // TreeMap
    float radius = 10;
    int numberOfAttempts = 5;
    float maximumHeightOfTrees = 46;
    float minimumHeightOfTrees = 10;
    int maximumNumberOfTrees = 16500;

    int currentTerrainVerticesInRow = 0;
    int currentTerrainLength = 0;
    int currentTerrainTextureRepetitionsInRow = 0;
    float currentTerrainFrequency = 8.0;
    int currentTerrainOctaves = 2;
    unsigned int currentTerrainSeed = 0;
    float creationTime = 0;

    int verticesInRow = 1024;
    int length = 1000;
    int textureRepetitionsInRow = 50;

private:
    unsigned int vao = 0;
    unsigned int vbo = 0;
    unsigned int ebo = 0;

    unsigned int lakeVao = 0;
    unsigned int lakeVbo = 0;

    short maximumTerrainHeight = 75;

    unsigned int *grassTextureDiffuseID, *grassTextureNormalID = nullptr;
    unsigned int *rockTextureDiffuseID, *rockTextureNormalID = nullptr;
    unsigned int *waterTextureDiffuseID;
    unsigned int *grassTextureID, *grassAlphaTextureID;

    Vertex* vertices;
    unsigned int* indices;

    Vertex* lakesVertices{};

    unsigned char* heightmap;
    unsigned char* forestMap;
    unsigned char* temporaryForestMap;
    unsigned char* largeForestMap;
    unsigned char* blurryLargeForestMap;
    glm::vec2* createdPoints;
    glm::vec2* activePoints;
    glm::vec3* grassPosition;
    glm::vec3* grassRotation;
    glm::vec3* flowersPosition;
    glm::vec3* flowersRotation;
    bool* lowVertices;
    bool* checkedVertices;

    int lakesVerticesCount = 0;
    float waterLevel = 30;
    int minimumVerticesCountToCreateLake = 600;
    int lakeVerticesCount;
    int gaussianBlurValue = 200;

public:
    ~Terrain();

    static Terrain* GetInstance();

    void Draw(Shader* shader);

    void DrawGrass();
    void DrawLakes() const;

    void CreateTerrain();

private:
    explicit Terrain();

    void CountingTheNumberOfVerticesInTheLake_Recursion(int index);
    void RemoveVerticesIfLakeIsSmall(int index);
};


#endif //DYNAMICZNYTEREN_TERRAIN_H
