#ifndef DYNAMICZNYTEREN_SHADER_H
#define DYNAMICZNYTEREN_SHADER_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#define shaderFilePath "res/shaders/"

class Shader {
public:
    unsigned int id;

    Shader(const char* vertexPath, const char* fragmentPath, const char* geometryPath = nullptr);

    virtual ~Shader();

    void Use() const;

    void SetBool(const std::string &name, bool value) const;
    void SetInt(const std::string &name, int value) const;
    void SetFloat(const std::string &name, float value) const;
    void SetVec3(const std::string &name, const glm::vec3 &value) const;
    void SetVec4(const std::string &name, const glm::vec4 &value) const;
    void SetMat4(const std::string &name, const glm::mat4 &mat) const;

private:
    static void CheckCompileErrors(unsigned int shader, const std::string& filename);
};

#endif //DYNAMICZNYTEREN_SHADER_H