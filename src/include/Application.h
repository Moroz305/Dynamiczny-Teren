#ifndef DYNAMICZNYTEREN_APPLICATION_H
#define DYNAMICZNYTEREN_APPLICATION_H

#include <glm/glm.hpp>

#define texturePath "res/textures/"
#define modelPath "res/models/"
#define fontPath "C:/Users/Kamil/Desktop/Dynamiczny Teren/res/fonts/"

class GLFWwindow;
class Shader;
class Model;

class Application {
public:
    inline static Application* application;

    Shader* basicShader{};
    Shader* terrainShader{};
    Shader* grassShader{};
    Shader* waterShader{};

    Model* leaves{};
    Model* trunk{};
    Model* grass{};
    Model* flowers{};

    glm::mat4 lightSpaceMatrix = glm::mat4(0);
    unsigned int depthMap = 0;
    int windowWidth = 1280, windowHeight = 720;

    int hour = 0;
    int day = 1;
    // Warsaw latitude in radians
    float latitudeInRadians = 0.911581456;

    bool renderTerrainEdges = false;
    bool renderLightVectors = false;

    bool terrainCreationWindow = false;

private:
    GLFWwindow* window = nullptr;

    float deltaTime = 0.0f;
    float lastFrame = 0.0f;

    float lastX = (float)windowWidth / 2.0f;
    float lastY = (float)windowHeight / 2.0f;

    bool firstMouse = true;

public:
    static Application* GetInstance();

    int Start();
    void Update();
    void End();

    static unsigned int LoadTexture(const char *filename, bool model = false);

private:
    Application();
    ~Application();

    static void GlfwErrorCallback(int error, const char* description);
    static void FramebufferSizeCallback([[maybe_unused]] GLFWwindow* window, int width, int height);
    static void MouseCallback([[maybe_unused]] GLFWwindow* window, double xposIn, double yposIn);
    static void ScrollCallback([[maybe_unused]] GLFWwindow* window, [[maybe_unused]] double xoffset, double yoffset);

    void ProcessInput();
};

#endif //DYNAMICZNYTEREN_APPLICATION_H