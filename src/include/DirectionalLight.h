#ifndef DYNAMICZNYTEREN_DIRECTIONALLIGHT_H
#define DYNAMICZNYTEREN_DIRECTIONALLIGHT_H

#include "Shader.h"

class DirectionalLight {
public:
    inline static DirectionalLight* directionalLight;

    glm::vec3 position = glm::vec3(5000.0f);
    glm::vec3 direction = glm::vec3(-1.0f);

    glm::vec3 ambient = glm::vec3(1.0f);
    glm::vec3 diffuse = glm::vec3(0.5f);
    glm::vec3 specular = glm::vec3(0.2f);

private:
    Shader shader;
    unsigned int linesVbo = 0;
    unsigned int linesVao = 0;
    unsigned int sunVbo = 0;
    unsigned int sunVao = 0;

    float linesVertices[6 * 51 * 51]{};
    float sunVertices[9216]{};

    float distance = 5000;

    float radius = 200;
    int sectorCount = 32;
    int stackCount = 16;

public:
    ~DirectionalLight();

    static DirectionalLight* GetInstance();

    void DrawLines();
    void DrawSun();

private:
    explicit DirectionalLight(const Shader &shader);

    void CreateSphere();
};

#endif //DYNAMICZNYTEREN_DIRECTIONALLIGHT_H