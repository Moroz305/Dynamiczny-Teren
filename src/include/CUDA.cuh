#ifndef DYNAMICZNYTEREN_CUDA_CUH
#define DYNAMICZNYTEREN_CUDA_CUH

#include <cuda_runtime.h>
#include "Mesh.h"

extern __global__ void CUDACreateHeightmap(unsigned char* output, int size, float frequency, int octaves, unsigned int seed, float mountainRatio, int mountainOctaves);
void CreateHeightmap(unsigned char* heightmap, unsigned int sizeOfTexture, float frequency, int octaves, unsigned int seed, float mountainRatio, int mountainOctaves);

extern __global__ void CUDACreateIndices(unsigned int* output, unsigned int size, int verticesInRow);
void CreateIndices(unsigned int* indices, unsigned int sizeOfIndices, int verticesInRow);

extern __global__ void CUDACreateNormalsAndTangents(Vertex* output, unsigned int size, unsigned int verticesInRow);
void CreateNormalsAndTangents(Vertex* vertices, unsigned int sizeOfVertices, unsigned int verticesInRow);

extern __global__ void CUDACreateForestMap(unsigned char* output, const unsigned char* forestMap, unsigned int size, int forestMapLength, int activeNeighboursRequired);
void CreateForestMap(unsigned char* forestMap, unsigned int sizeOfTexture, int forestMapLength, int iterations, int activeNeighboursRequired[5]);

#endif //DYNAMICZNYTEREN_CUDA_CUH