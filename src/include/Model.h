#ifndef DYNAMICZNYTEREN_MODEL_H
#define DYNAMICZNYTEREN_MODEL_H

#include <assimp/scene.h>
#include <string>
#include <vector>
#include "include/Application.h"
#include "include/Mesh.h"

#define modelPath "res/models/"

class Shader;

class Model {
public:
    // model data
    std::vector<Texture> texturesLoaded;	// stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
    std::vector<Mesh> meshes;
    glm::mat4* modelMatrices;
    unsigned int instancesAmount;
    bool isItVegetation;

    explicit Model(std::string const &filename, unsigned int instancesAmount = 1, bool isItVegetation = false);
    ~Model();

    void Draw(Shader* shader = Application::GetInstance()->basicShader);

    void SendModelMatrices();

private:
    void LoadModel(std::string const &path);
    void ProcessNode(aiNode *node, const aiScene *scene);
    Mesh ProcessMesh(aiMesh *mesh, const aiScene *scene);
    std::vector<Texture> LoadMaterialTextures(aiMaterial *mat, aiTextureType type, const std::string& typeName);
};

#endif //DYNAMICZNYTEREN_MODEL_H