#include "include/Application.h"

int main(int, char**)
{
    // Setup window
    if (Application::GetInstance()->Start() == 1)
        return 1;

    // Main loop
    Application::GetInstance()->Update();

    // Cleanup
    Application::GetInstance()->End();

    return 0;
}