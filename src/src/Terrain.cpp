#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include <random>
#include "include/Terrain.h"
#include "include/Application.h"
#include "include/Model.h"
#include "GLFW/glfw3.h"
#include "include/CUDA.cuh"

#define PI 3.14159265358979323846

#pragma clang diagnostic push
#pragma ide diagnostic ignored "NullDereference"
Terrain::Terrain() {
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    glGenVertexArrays(1, &lakeVao);
    glGenBuffers(1, &lakeVbo);

    Shader* shader = Application::GetInstance()->terrainShader;

    shader->Use();
    grassTextureDiffuseID = new unsigned int;
    *grassTextureDiffuseID = Application::LoadTexture("grass.png");
    shader->SetInt("grass_texture_diffuse", 1);

    rockTextureDiffuseID = new unsigned int;
    *rockTextureDiffuseID = Application::LoadTexture("rock.jpg");
    shader->SetInt("rock_texture_diffuse", 3);

    Application::GetInstance()->grassShader->Use();
    grassTextureID = new unsigned int;
    *grassTextureID = Application::LoadTexture("grass.jpg");
    Application::GetInstance()->grassShader->SetInt("grass_texture", 0);

    grassAlphaTextureID = new unsigned int;
    *grassAlphaTextureID = Application::LoadTexture("grass_alpha.jpg");
    Application::GetInstance()->grassShader->SetInt("grass_alpha_texture", 1);

    Application::GetInstance()->waterShader->Use();
    waterTextureDiffuseID = new unsigned int;
    *waterTextureDiffuseID = Application::LoadTexture("water.jpg");
    Application::GetInstance()->waterShader->SetInt("water_texture_diffuse", 0);

    vertices = new Vertex[1024 * 1024];
    indices = new unsigned int[(1024 - 1) * (1024 - 1) * 6];
    heightmap = new unsigned char[1024 * 1024];
    forestMap = new unsigned char[64 * 64];
    temporaryForestMap = new unsigned char[64 * 64];
    largeForestMap = new unsigned char[1024 * 1024];
    blurryLargeForestMap = new unsigned char[1024 * 1024];
    createdPoints = new glm::vec2[maximumNumberOfTrees];
    grassPosition = new glm::vec3[1024 * 1024 / 2];
    grassRotation = new glm::vec3[1024 * 1024 / 2];
    flowersPosition = new glm::vec3[1024 * 1024 / 2];
    flowersRotation = new glm::vec3[1024 * 1024 / 2];
    activePoints = new glm::vec2[20000];
    lowVertices = new bool[1024 * 1024]{false};
    checkedVertices = new bool[1024 * 1024]{false};
    CreateTerrain();
}
#pragma clang diagnostic pop

Terrain::~Terrain() {
    delete[] vertices;
    delete[] indices;
    delete[] heightmap;
    delete[] forestMap;
    delete[] temporaryForestMap;
    delete[] largeForestMap;
    delete[] blurryLargeForestMap;
    delete[] createdPoints;
    delete[] activePoints;
    delete[] grassPosition;
    delete[] grassRotation;
    delete[] flowersPosition;
    delete[] flowersRotation;
    delete[] lakesVertices;
    delete[] lowVertices;
    delete[] checkedVertices;
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &ebo);
    glDeleteVertexArrays(1, &lakeVao);
    glDeleteBuffers(1, &lakeVbo);
}

Terrain *Terrain::GetInstance() {
    if (terrain == nullptr) {
        terrain = new Terrain();
    }
    return terrain;
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "NullDereference"
#pragma ide diagnostic ignored "ConstantConditionsOC"
#pragma ide diagnostic ignored "UnreachableCode"
void Terrain::Draw(Shader* shader) {
    shader->Use();
    shader->SetInt("maximumTerrainHeight", maximumTerrainHeight);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Application::application->depthMap);
    // Grass texture
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, *grassTextureDiffuseID);
    if (grassTextureNormalID) {
        shader->SetBool("has_grass_texture_normal", true);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, *grassTextureNormalID);
    } else
        shader->SetBool("has_grass_texture_normal", false);
    // Rock texture
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, *rockTextureDiffuseID);
    if (rockTextureNormalID) {
        shader->SetBool("has_rock_texture_normal", true);
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, *rockTextureNormalID);
    } else
        shader->SetBool("has_rock_texture_normal", false);

    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, (currentTerrainVerticesInRow - 1) * (currentTerrainVerticesInRow - 1) * 6, GL_UNSIGNED_INT, nullptr);

    if (Application::GetInstance()->renderTerrainEdges) {
        shader->SetBool("debug", true);
        glDepthFunc(GL_LEQUAL);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glDrawElements(GL_TRIANGLES, (currentTerrainVerticesInRow - 1) * (currentTerrainVerticesInRow - 1) * 6, GL_UNSIGNED_INT, nullptr);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glDepthFunc(GL_LESS);
        shader->SetBool("debug", false);
    }
    glBindVertexArray(0);

    for (int i = 0; i < 16; i++) {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
#pragma clang diagnostic pop

void Terrain::DrawGrass() {
    auto application = Application::GetInstance();
    application->grassShader->Use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, *grassTextureID);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, *grassAlphaTextureID);

    glBindVertexArray(application->grass->meshes[0].vao);
    glDrawElementsInstanced(GL_TRIANGLES, (int)application->grass->meshes[0].indices.size(), GL_UNSIGNED_INT, nullptr, (int)application->grass->instancesAmount);
    glBindVertexArray(0);

    for (int i = 0; i < 16; i++) {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "NullDereference"
void Terrain::DrawLakes() const {
    Application::GetInstance()->waterShader->Use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, *waterTextureDiffuseID);
    glBindVertexArray(lakeVao);
    glDrawArrays(GL_TRIANGLES, 0, lakesVerticesCount);
    glBindVertexArray(0);

    for (int i = 0; i < 16; i++) {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
#pragma clang diagnostic pop

#pragma clang diagnostic push
#pragma ide diagnostic ignored "NullDereference"
void Terrain::CreateTerrain() {
    auto time = (float)glfwGetTime();

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<float>Random_0_1(0.0, 1.0);
    std::uniform_real_distribution<float>Random_radius_2radius(radius, 2 * radius);
    std::uniform_real_distribution<float>Random_0_360(0.0, 360.0);
    std::uniform_real_distribution<float>Random_0i75_1i25(0.75, 1.25);

    glDeleteTextures(1, &heightMapID);
    glDeleteTextures(1, &forestMapID);

    Application::GetInstance()->terrainCreationWindow = true;
    currentTerrainVerticesInRow = verticesInRow;
    currentTerrainLength = length;
    currentTerrainTextureRepetitionsInRow = textureRepetitionsInRow;
    currentTerrainFrequency = frequency;
    currentTerrainOctaves = octaves;
    currentTerrainSeed = seed;

    lakesVerticesCount = 0;

    // Create heightmap
    CreateHeightmap(heightmap, heightmapLength * heightmapLength, frequency, octaves, seed, maximumHeightOfTrees / (float)maximumTerrainHeight, mountainOctaves);

    // Applying a Laplacian smoothing
    int smoothIterations = 3;
    auto* tempMap = new unsigned char[verticesInRow * verticesInRow];
    std::memcpy(tempMap, heightmap, verticesInRow * verticesInRow);
    for (int iter = 0; iter < smoothIterations; ++iter) {
        for (int row = 1; row < verticesInRow - 1; ++row) {
            for (int col = 1; col < verticesInRow - 1; ++col) {
                int idx = row * verticesInRow + col;
                heightmap[idx] = static_cast<unsigned char>((tempMap[idx - verticesInRow] + tempMap[idx + verticesInRow] +
                                                             tempMap[idx - 1] + tempMap[idx + 1]) / 4.0);
            }
        }
    }
    delete[] tempMap;

    // Create forestMap with Cellular Automata
    for (int i = 0, size = forestMapLength * forestMapLength; i < size; ++i) {
        float chance = Random_0_1(mt);
        if (chance < probability)
            forestMap[i] = 255;
        else
            forestMap[i] = 0;
    }

    CreateForestMap(forestMap, forestMapLength * forestMapLength, forestMapLength, iterations, activeNeighboursRequired);

    // Create terrain's mesh
    float textureLengthBetweenVertices = static_cast<float>(textureRepetitionsInRow) / static_cast<float>(verticesInRow - 1);
    float distanceBetweenVertices = static_cast<float>(length) / static_cast<float>(verticesInRow - 1);
    float halfLength = static_cast<float>(length) / 2;
    float invMaxHeight = 1.0f / 255.0f * static_cast<float>(maximumTerrainHeight);
    int increaseRatio = verticesInRow / forestMapLength;

    for (int i = 0, size = verticesInRow * verticesInRow; i < size; ++i) {
        unsigned int x = i % verticesInRow;
        unsigned int y = i / verticesInRow;

        Vertex& vertex = vertices[i];

        vertex.position.x = static_cast<float>(x) * distanceBetweenVertices - halfLength;
        vertex.position.y = static_cast<float>(heightmap[i]) * invMaxHeight;
        vertex.position.z = static_cast<float>(y) * distanceBetweenVertices - halfLength;

        vertex.texCoords.x = textureLengthBetweenVertices * static_cast<float>(x);
        vertex.texCoords.y = textureLengthBetweenVertices * static_cast<float>(y);

        if (heightmap[i] <= waterLevel) {
            lowVertices[i] = true;
            checkedVertices[i] = false;
        }
        else {
            lowVertices[i] = false;
            checkedVertices[i] = true;
        }

        if (x % increaseRatio == 0  &&  y % increaseRatio == 0) {
            for (int j = i, size2 = i + increaseRatio * increaseRatio; j < size2; ++j) {
                largeForestMap[(y + (j - i) / increaseRatio) * verticesInRow + x + (j - i) % increaseRatio] = forestMap[y / increaseRatio * forestMapLength + x / increaseRatio];
            }
        }
    }

    CreateNormalsAndTangents(vertices, (verticesInRow - 1) * (verticesInRow - 1), verticesInRow);

    // Generate Gaussian kernel
    int kernelSize = 17;
    std::vector<float> kernel(kernelSize * kernelSize);

    float sigma = 1000.0f;
    float sum = 0.0f;

    for (int y = 0; y < kernelSize; ++y) {
        for (int x = 0; x < kernelSize; ++x) {
            int dx = x - kernelSize / 2;
            int dy = y - kernelSize / 2;
            kernel[y * kernelSize + x] = std::exp(-(dx * dx + dy * dy) / (2 * sigma * sigma));
            sum += kernel[y * kernelSize + x];
        }
    }

    // Normalize kernel
    for (int i = 0; i < kernelSize * kernelSize; ++i) {
        kernel[i] /= sum;
    }

    // Gaussian Blur
    for (int y = 0; y < verticesInRow; ++y) {
        for (int x = 0; x < verticesInRow; ++x) {
            float value = 0.0f;

            for (int ky = 0; ky < kernelSize; ++ky) {
                for (int kx = 0; kx < kernelSize; ++kx) {
                    int nx = std::min(std::max(x + kx - kernelSize / 2, 0), verticesInRow - 1);
                    int ny = std::min(std::max(y + ky - kernelSize / 2, 0), verticesInRow - 1);

                    value += largeForestMap[ny * verticesInRow + nx] * kernel[ky * kernelSize + kx];
                }
            }

            if (value < gaussianBlurValue)
                blurryLargeForestMap[y * verticesInRow + x] = 0;
            else
                blurryLargeForestMap[y * verticesInRow + x] = 255;
        }
    }

    int numberOfGrass = 0;
    int numberOfFlowers = 0;

    for (int i = 0, size = verticesInRow * verticesInRow; i < size; ++i) {
        unsigned int x = i % verticesInRow;
        unsigned int y = i / verticesInRow;

        Vertex& vertex = vertices[i];
        glm::vec3& normal = vertex.normal;
        glm::vec3& tangent = vertex.tangent;
        glm::vec3& bitangent = vertex.bitangent;

        normal = glm::normalize(normal);
        tangent = glm::normalize(tangent);

        bitangent.x = normal.y * tangent.z - normal.z * tangent.y;
        bitangent.y = normal.z * tangent.x - normal.x * tangent.z;
        bitangent.z = normal.x * tangent.y - normal.y * tangent.x;

        if (lowVertices[i] && !checkedVertices[i]) {
            lakeVerticesCount = 0;
            CountingTheNumberOfVerticesInTheLake_Recursion(i);
            if (lakeVerticesCount < minimumVerticesCountToCreateLake)
                RemoveVerticesIfLakeIsSmall(i);
        }

        // If the triangles are low, there will be a lake there
        if (i % verticesInRow != verticesInRow - 1  &&  i / verticesInRow != verticesInRow - 1) {
            if (lowVertices[i]) {
                if (lowVertices[i + 1] && lowVertices[i + verticesInRow + 1])
                    lakesVerticesCount += 3;
                if (lowVertices[i + verticesInRow] && lowVertices[i + verticesInRow + 1])
                    lakesVerticesCount += 3;
            }
        }

        if (x >= 2  &&  y >= 2  &&  x <= verticesInRow - 2  &&  y <= verticesInRow - 2  &&  x % 2 == 1  &&  y % 2 == 1  &&  vertex.position.y > minimumHeightOfTrees - 1  &&  vertex.position.y < maximumHeightOfTrees - 2  &&
            !blurryLargeForestMap[i - verticesInRow - verticesInRow - 2]  &&  !blurryLargeForestMap[i - verticesInRow - verticesInRow - 1]  &&  !blurryLargeForestMap[i - verticesInRow - verticesInRow]  &&
            !blurryLargeForestMap[i - verticesInRow - verticesInRow + 1]  &&  !blurryLargeForestMap[i - verticesInRow - verticesInRow + 2]  &&  !blurryLargeForestMap[i - verticesInRow - 2]  &&
            !blurryLargeForestMap[i - verticesInRow + 2]  &&  !blurryLargeForestMap[i - 2]  &&  !blurryLargeForestMap[i + 2]  &&  !blurryLargeForestMap[i + verticesInRow - 2]  &&  !blurryLargeForestMap[i + verticesInRow + 2]  &&
            !blurryLargeForestMap[i + verticesInRow + verticesInRow - 2]  &&  !blurryLargeForestMap[i + verticesInRow + verticesInRow - 1]  &&  !blurryLargeForestMap[i + verticesInRow + verticesInRow]  &&
            !blurryLargeForestMap[i + verticesInRow + verticesInRow + 1]  &&  !blurryLargeForestMap[i + verticesInRow + verticesInRow + 2]) {
                grassPosition[numberOfGrass] = glm::vec3(vertex.position);

                float angleX, angleZ;
                // X rotation
                glm::vec3 vector1 = vertex.position - vertices[i - verticesInRow].position;
                if (vector1.y < 0)
                    angleX = glm::acos(glm::dot(vector1, glm::vec3(0, 0, 1)) / glm::length(vector1));
                else
                    angleX = 2 * PI - glm::acos(glm::dot(vector1, glm::vec3(0, 0, 1)) / glm::length(vector1));
                // Z rotation
                glm::vec3 vector2 = vertex.position - vertices[i - 1].position;
                if (vector2.y > 0)
                    angleZ = glm::acos(glm::dot(vector2, glm::vec3(1, 0, 0)) / glm::length(vector2));
                else
                    angleZ = 2 * PI - glm::acos(glm::dot(vector2, glm::vec3(1, 0, 0)) / glm::length(vector2));

                grassRotation[numberOfGrass] = glm::vec3(angleX, 0.0f, angleZ);
                numberOfGrass++;

                float chance = Random_0_1(mt);
                if (chance < 0.025f) {
                    flowersPosition[numberOfFlowers] = glm::vec3(vertex.position);
                    flowersRotation[numberOfFlowers] = grassRotation[numberOfGrass - 1];
                    numberOfFlowers++;
                }
        }
    }

    CreateIndices(indices, (verticesInRow - 1) * (verticesInRow - 1) * 6, verticesInRow);

    // Create treeMap with Poisson disk sampling
    int numberOfTrees = 1;

    float cellSize = radius / sqrtf(2);
    int gridSize = ceil(static_cast<float>(length) / cellSize);
    float heightmapCellSize = static_cast<float>(length) / static_cast<float>(heightmapLength);

    auto* grid = new unsigned char[gridSize * gridSize]{0};

    auto centerPoint = glm::vec2(0);
    while (static_cast<float>(heightmap[static_cast<int>(halfLength / heightmapCellSize) * heightmapLength + static_cast<int>((centerPoint.x + halfLength) / heightmapCellSize)]) * invMaxHeight > maximumHeightOfTrees ||
           static_cast<float>(heightmap[static_cast<int>(halfLength / heightmapCellSize) * heightmapLength + static_cast<int>((centerPoint.x + halfLength) / heightmapCellSize)]) * invMaxHeight < minimumHeightOfTrees) {
        centerPoint = glm::vec2(centerPoint.x - 1, 0);
    }
    grid[static_cast<int>(halfLength / cellSize) * gridSize + static_cast<int>((centerPoint.x + halfLength) / cellSize)] = 255;
    createdPoints[0] = centerPoint;
    activePoints[0] = centerPoint;

    for (int i = 0; i < 20000 && i < numberOfTrees && numberOfTrees < maximumNumberOfTrees; ++i) {
        for (int j = 0; j < numberOfAttempts && numberOfTrees < maximumNumberOfTrees; ++j) {
            auto angle = static_cast<float>(Random_0_1(mt) * PI * 2);

            glm::vec2 direction = glm::vec2(sin(angle), cos(angle));

            glm::vec2 newPoint = glm::vec2(activePoints[i] + direction * Random_radius_2radius(mt));

            if (newPoint.x > -halfLength  &&  newPoint.x < halfLength  &&  newPoint.y > -halfLength  &&  newPoint.y < halfLength) {
                int index = static_cast<int>((newPoint.y + halfLength) / cellSize) * gridSize + static_cast<int>((newPoint.x + halfLength) / cellSize);
                int heightmapIndex = static_cast<int>((newPoint.y + halfLength) / heightmapCellSize) * heightmapLength + static_cast<int>((newPoint.x + halfLength) / heightmapCellSize);
                if (!grid[index] && static_cast<float>(heightmap[heightmapIndex]) * invMaxHeight < maximumHeightOfTrees && static_cast<float>(heightmap[heightmapIndex]) * invMaxHeight > minimumHeightOfTrees) {
                    createdPoints[numberOfTrees] = newPoint;
                    activePoints[numberOfTrees] = newPoint;
                    grid[index] = 255;
                    numberOfTrees++;
                }
            }
        }
    }

    delete[] grid;


    // Create trees
    Model* trunk = Application::GetInstance()->trunk;
    Model* leaves = Application::GetInstance()->leaves;
    Model* grass = Application::GetInstance()->grass;
    Model* flowers = Application::GetInstance()->flowers;

    if (trunk) {
        if (trunk->instancesAmount != 0)
            delete[] trunk->modelMatrices;
        if (leaves->instancesAmount != 0)
            delete[] leaves->modelMatrices;
        if (grass->instancesAmount != 0)
            delete[] grass->modelMatrices;
        if (flowers->instancesAmount != 0)
            delete[] flowers->modelMatrices;

        trunk->instancesAmount = numberOfTrees;
        leaves->instancesAmount = numberOfTrees;
        grass->instancesAmount = numberOfGrass;
        flowers->instancesAmount = numberOfFlowers;

        trunk->modelMatrices = new glm::mat4[trunk->instancesAmount];
        leaves->modelMatrices = new glm::mat4[leaves->instancesAmount];
        grass->modelMatrices = new glm::mat4[grass->instancesAmount];
        flowers->modelMatrices = new glm::mat4[flowers->instancesAmount];

        for (int i = 0; i < numberOfTrees; ++i) {
            float x = (createdPoints[i].x + halfLength) / heightmapCellSize;
            int texturesIndex = static_cast<int>((createdPoints[i].y + halfLength) / heightmapCellSize) * heightmapLength + static_cast<int>(x);
            if (!blurryLargeForestMap[texturesIndex]) {
                trunk->modelMatrices[i] = glm::translate(glm::mat4(1.0f), glm::vec3(0, -10, 0));
                leaves->modelMatrices[i] = glm::translate(glm::mat4(1.0f), glm::vec3(0, -10, 0));
                continue;
            }

            auto scale = glm::vec3(Random_0i75_1i25(mt));
            float rotationY = Random_0_360(mt);

            float closerVertex = x - floor(x);
            float interpolateHeight = (1 - closerVertex) * static_cast<float>(heightmap[texturesIndex]) + closerVertex * static_cast<float>(heightmap[texturesIndex + 1]);
            trunk->modelMatrices[i] = glm::translate(glm::mat4(1.0f), glm::vec3(createdPoints[i].x, interpolateHeight * invMaxHeight - 0.25f, createdPoints[i].y)) *
                                      glm::rotate(glm::mat4(1.0f), glm::radians(rotationY), glm::vec3(0.0f, 1.0f, 0.0f)) *
                                      glm::scale(glm::mat4(1.0f), scale);
            leaves->modelMatrices[i] = glm::translate(glm::mat4(1.0f), glm::vec3(createdPoints[i].x, interpolateHeight * invMaxHeight - 0.25f, createdPoints[i].y)) *
                                      glm::rotate(glm::mat4(1.0f), glm::radians(rotationY), glm::vec3(0.0f, 1.0f, 0.0f)) *
                                      glm::scale(glm::mat4(1.0f), scale);
        }

        for (int i = 0; i < numberOfGrass; ++i) {
            grass->modelMatrices[i] = glm::translate(glm::mat4(1.0f), grassPosition[i]) *
                                      glm::rotate(glm::mat4(1.0f), grassRotation[i].x, glm::vec3(1.0f, 0.0f, 0.0f)) *
                                      glm::rotate(glm::mat4(1.0f), grassRotation[i].z, glm::vec3(0.0f, 0.0f, 1.0f)) *
                                      glm::scale(glm::mat4(1.0f), glm::vec3(0.0045f));
        }

        for (int i = 0; i < numberOfFlowers; ++i) {
            flowers->modelMatrices[i] = glm::translate(glm::mat4(1.0f), flowersPosition[i]) *
                                        glm::rotate(glm::mat4(1.0f), flowersRotation[i].x, glm::vec3(1.0f, 0.0f, 0.0f)) *
                                        glm::rotate(glm::mat4(1.0f), flowersRotation[i].z, glm::vec3(0.0f, 0.0f, 1.0f)) *
                                        glm::scale(glm::mat4(1.0f), glm::vec3(0.02f));
        }

        trunk->SendModelMatrices();
        leaves->SendModelMatrices();
        grass->SendModelMatrices();
        flowers->SendModelMatrices();
    }

    // Create lakes
    delete[] lakesVertices;
    lakesVertices = new Vertex[lakesVerticesCount];
    float lakeHeight = waterLevel * invMaxHeight;

    for (int i = 0, size = (verticesInRow - 1) * (verticesInRow - 1), iterator = 0; i < size; ++i) {
        unsigned int x = i % (verticesInRow - 1);
        unsigned int y = i / (verticesInRow - 1);
        unsigned int idx = y * verticesInRow + x;

        if (lowVertices[idx]) {
            glm::vec3 firstVertexPosition = vertices[idx].position;
            glm::vec3 secondVertexPosition = vertices[idx + 1].position;
            glm::vec3 thirdVertexPosition = vertices[idx + verticesInRow].position;
            glm::vec3 fourthVertexPosition = vertices[idx + verticesInRow + 1].position;

            // First triangle
            if (lowVertices[idx + 1] && lowVertices[idx + verticesInRow + 1]) {
                lakesVertices[iterator].position = glm::vec3(firstVertexPosition.x, lakeHeight - 1, firstVertexPosition.z);
                lakesVertices[iterator + 1].position = glm::vec3(fourthVertexPosition.x, lakeHeight - 1, fourthVertexPosition.z);
                lakesVertices[iterator + 2].position = glm::vec3(secondVertexPosition.x, lakeHeight - 1, secondVertexPosition.z);
                lakesVertices[iterator].normal = glm::vec3(0, 1, 0);
                lakesVertices[iterator + 1].normal = glm::vec3(0, 1, 0);
                lakesVertices[iterator + 2].normal = glm::vec3(0, 1, 0);
                lakesVertices[iterator].texCoords = glm::vec2(textureLengthBetweenVertices * (float)x, textureLengthBetweenVertices * (float)y);
                lakesVertices[iterator + 1].texCoords = glm::vec2(textureLengthBetweenVertices * (float)(x + 1), textureLengthBetweenVertices * (float)(y + 1));
                lakesVertices[iterator + 2].texCoords = glm::vec2(textureLengthBetweenVertices * (float)(x + 1), textureLengthBetweenVertices * (float)y);
                iterator += 3;
            }
            // Second triangle
            if (lowVertices[idx + verticesInRow] && lowVertices[idx + verticesInRow + 1]) {
                lakesVertices[iterator].position = glm::vec3(firstVertexPosition.x, lakeHeight - 1, firstVertexPosition.z);
                lakesVertices[iterator + 1].position = glm::vec3(thirdVertexPosition.x, lakeHeight - 1, thirdVertexPosition.z);
                lakesVertices[iterator + 2].position = glm::vec3(fourthVertexPosition.x, lakeHeight - 1, fourthVertexPosition.z);
                lakesVertices[iterator].normal = glm::vec3(0, 1, 0);
                lakesVertices[iterator + 1].normal = glm::vec3(0, 1, 0);
                lakesVertices[iterator + 2].normal = glm::vec3(0, 1, 0);
                lakesVertices[iterator].texCoords = glm::vec2(textureLengthBetweenVertices * (float)x, textureLengthBetweenVertices * (float)y);
                lakesVertices[iterator + 1].texCoords = glm::vec2(textureLengthBetweenVertices * (float)x, textureLengthBetweenVertices * (float)(y + 1));
                lakesVertices[iterator + 2].texCoords = glm::vec2(textureLengthBetweenVertices * (float)(x + 1), textureLengthBetweenVertices * (float)(y + 1));
                iterator += 3;
            }
        }
    }


    glGenTextures(1, &heightMapID);
    glBindTexture(GL_TEXTURE_2D, heightMapID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, heightmapLength, heightmapLength, 0, GL_RED, GL_UNSIGNED_BYTE, heightmap);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, GL_RED);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_RED);

    glGenTextures(1, &forestMapID);
    glBindTexture(GL_TEXTURE_2D, forestMapID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, verticesInRow, verticesInRow, 0, GL_GREEN, GL_UNSIGNED_BYTE, blurryLargeForestMap);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    if (!trunk)
        glBufferData(GL_ARRAY_BUFFER, (long long)sizeof(Vertex) * verticesInRow * verticesInRow, vertices, GL_STATIC_DRAW);
    else
        glBufferSubData(GL_ARRAY_BUFFER, 0, (long long)sizeof(Vertex) * verticesInRow * verticesInRow, vertices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    if (!trunk)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, (long long)((verticesInRow - 1) * (verticesInRow - 1) * 6 * sizeof(unsigned int)), indices, GL_STATIC_DRAW);
    else
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, (long long)((verticesInRow - 1) * (verticesInRow - 1) * 6 * sizeof(unsigned int)), indices);
    // Set the vertex attribute pointers
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), nullptr);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoords));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bitangent));
    glEnableVertexAttribArray(4);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glBindVertexArray(lakeVao);
    glBindBuffer(GL_ARRAY_BUFFER, lakeVbo);
    glBufferData(GL_ARRAY_BUFFER, (long long)sizeof(Vertex) * lakesVerticesCount, lakesVertices, GL_STATIC_DRAW);
    // Set the vertex attribute pointers
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), nullptr);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoords));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bitangent));
    glEnableVertexAttribArray(4);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    creationTime = (float)glfwGetTime() - time;
}

void Terrain::CountingTheNumberOfVerticesInTheLake_Recursion(int index) {
    checkedVertices[index] = true;
    lakeVerticesCount += 1;
    if (!checkedVertices[index - verticesInRow - 1])
        CountingTheNumberOfVerticesInTheLake_Recursion(index - verticesInRow - 1);
    if (!checkedVertices[index - verticesInRow])
        CountingTheNumberOfVerticesInTheLake_Recursion(index - verticesInRow);
    if (!checkedVertices[index - verticesInRow + 1])
        CountingTheNumberOfVerticesInTheLake_Recursion(index - verticesInRow + 1);
    if (!checkedVertices[index - 1])
        CountingTheNumberOfVerticesInTheLake_Recursion(index - 1);
    if (!checkedVertices[index + 1])
        CountingTheNumberOfVerticesInTheLake_Recursion(index + 1);
    if (!checkedVertices[index + verticesInRow - 1])
        CountingTheNumberOfVerticesInTheLake_Recursion(index + verticesInRow - 1);
    if (!checkedVertices[index + verticesInRow])
        CountingTheNumberOfVerticesInTheLake_Recursion(index + verticesInRow);
    if (!checkedVertices[index + verticesInRow + 1])
        CountingTheNumberOfVerticesInTheLake_Recursion(index + verticesInRow + 1);
}

void Terrain::RemoveVerticesIfLakeIsSmall(int index) {
    lowVertices[index] = false;
    if (lowVertices[index - verticesInRow - 1])
        RemoveVerticesIfLakeIsSmall(index - verticesInRow - 1);
    if (lowVertices[index - verticesInRow])
        RemoveVerticesIfLakeIsSmall(index - verticesInRow);
    if (lowVertices[index - verticesInRow + 1])
        RemoveVerticesIfLakeIsSmall(index - verticesInRow + 1);
    if (lowVertices[index - 1])
        RemoveVerticesIfLakeIsSmall(index - 1);
    if (lowVertices[index + 1])
        RemoveVerticesIfLakeIsSmall(index + 1);
    if (lowVertices[index + verticesInRow - 1])
        RemoveVerticesIfLakeIsSmall(index + verticesInRow - 1);
    if (lowVertices[index + verticesInRow])
        RemoveVerticesIfLakeIsSmall(index + verticesInRow);
    if (lowVertices[index + verticesInRow + 1])
        RemoveVerticesIfLakeIsSmall(index + verticesInRow + 1);
}

#pragma clang diagnostic pop