#include <glm/gtc/matrix_transform.hpp>
#include "include/Camera.h"

Camera::Camera() {
    UpdateCameraVectors();
}

Camera::~Camera() = default;

Camera *Camera::GetInstance() {
    if (camera == nullptr) {
        camera = new Camera();
    }
    return camera;
}

glm::mat4 Camera::GetViewMatrix() const {
    return glm::lookAt(position, position + front, up);
}

void Camera::ProcessKeyboard(Camera_Movement direction, float deltaTime) {
    float velocity = movementSpeed * deltaTime;
    if (direction == FORWARD)
        position += glm::vec3(0, 0, -1) * velocity;
    if (direction == BACKWARD)
        position += glm::vec3(0, 0, 1) * velocity;
    if (direction == LEFT)
        position += glm::vec3(-1, 0, 0) * velocity;
    if (direction == RIGHT)
        position += glm::vec3(1, 0, 0) * velocity;
}

void Camera::ProcessMouseMovement(float xoffset, float yoffset) {
    xoffset *= mouseSensitivity;
    yoffset *= mouseSensitivity;

    yaw += xoffset;
    pitch += yoffset;

    if (pitch > 89.0f)
        pitch = 89.0f;
    if (pitch < -89.0f)
        pitch = -89.0f;

    UpdateCameraVectors();
}

void Camera::ProcessZoom(float yoffset) {
    zoom -= yoffset;
    if (zoom < 1.0f)
        zoom = 1.0f;
    if (zoom > 10.0f)
        zoom = 10.0f;
}

void Camera::ProcessMouseScroll(float yoffset) {
    position.y += yoffset;
}

void Camera::UpdateCameraVectors() {
    glm::vec3 front2;
    front2.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    front2.y = sin(glm::radians(pitch));
    front2.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    front = glm::normalize(front2);
    right = glm::normalize(glm::cross(front, worldUp));
    up = glm::normalize(glm::cross(right, front));
}