#include <glm/gtc/matrix_transform.hpp>
#include "include/DirectionalLight.h"
#include "include/Camera.h"
#include "include/Application.h"

#define PI 3.14159265358979323846

DirectionalLight::DirectionalLight(const Shader &shader) : shader(shader) {
    int vectorsOfLightInRow = (int)sqrt(sizeof(linesVertices) / sizeof(float) / 6);
    float distanceBetweenVectors = 2000.0f / (float)(vectorsOfLightInRow - 1);

    for (int i = 0; i <= vectorsOfLightInRow - 1; i++) {
        for (int j = 0; j <= vectorsOfLightInRow - 1; j++) {
            linesVertices[6 * (i * vectorsOfLightInRow + j)] = -1000.0f + (float)i * distanceBetweenVectors;
            linesVertices[6 * (i * vectorsOfLightInRow + j) + 2] = -1000.0f + (float)j * distanceBetweenVectors;
            linesVertices[6 * (i * vectorsOfLightInRow + j) + 3] = -1000.0f + (float)i * distanceBetweenVectors;
            linesVertices[6 * (i * vectorsOfLightInRow + j) + 4] = 1.0f;
            linesVertices[6 * (i * vectorsOfLightInRow + j) + 5] = -1000.0f + (float)j * distanceBetweenVectors;
        }
    }

    CreateSphere();

    glGenVertexArrays(1, &linesVao);
    glGenBuffers(1, &linesVbo);
    glBindVertexArray(linesVao);
    glBindBuffer(GL_ARRAY_BUFFER, linesVbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(linesVertices), linesVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glGenVertexArrays(1, &sunVao);
    glGenBuffers(1, &sunVbo);
    glBindVertexArray(sunVao);
    glBindBuffer(GL_ARRAY_BUFFER, sunVbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sunVertices), sunVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    shader.Use();
    shader.SetMat4("view", Camera::GetInstance()->GetViewMatrix());
    shader.SetMat4("projection", glm::perspective(glm::radians(Camera::camera->zoom), (float)Application::GetInstance()->windowWidth / (float)Application::GetInstance()->windowHeight, 1.0f, 200.0f));
}

DirectionalLight::~DirectionalLight() {
    glDeleteVertexArrays(1, &linesVao);
    glDeleteBuffers(1, &linesVbo);
    glDeleteVertexArrays(1, &sunVao);
    glDeleteBuffers(1, &sunVbo);
    glDeleteProgram(shader.id);
}

DirectionalLight *DirectionalLight::GetInstance() {
    if (directionalLight == nullptr) {
        directionalLight = new DirectionalLight(Shader("light.vert", "light.frag"));
    }
    return directionalLight;
}

void DirectionalLight::DrawLines() {
    shader.Use();
    shader.SetMat4("view", Camera::camera->GetViewMatrix());
    shader.SetMat4("projection", glm::perspective(glm::radians(Camera::camera->zoom), (float)Application::GetInstance()->windowWidth / (float)Application::GetInstance()->windowHeight, 1.0f, 200.0f));
    shader.SetVec3("lightPosition", position);
    shader.SetVec4("color", glm::vec4(1.0, 1.0, 1.0, 0.3));
    shader.SetBool("isSun", false);

    glBindVertexArray(linesVao);
    glDrawArrays(GL_LINES, 0, sizeof(linesVertices) / sizeof(float) / 3);
    glBindVertexArray(0);
}

void DirectionalLight::DrawSun() {
    // Calculate sun position
    Application* application = Application::GetInstance()->application;
    float hourAngleInRadians = (application->hour - 12) * 15 * PI / 180;
    float declinationInRadians = 23.44 * sin(360.0f / 365 * (application->day - 81) * PI / 180) * PI / 180;

    float cosinusOfSolarZenithAngle = sin(application->latitudeInRadians) * sin(declinationInRadians) + cos(application->latitudeInRadians) * cos(declinationInRadians) * cos(hourAngleInRadians);
    cosinusOfSolarZenithAngle = glm::clamp(cosinusOfSolarZenithAngle, -1.0f, 1.0f);
    float solarZenithAngleInRadians = acos(cosinusOfSolarZenithAngle);

    float cosinusOfSolarAzimuthAngle = (sin(declinationInRadians) * cos(application->latitudeInRadians) - cos(hourAngleInRadians) * cos(declinationInRadians) * sin(application->latitudeInRadians)) /
                                        sin(solarZenithAngleInRadians);
    cosinusOfSolarAzimuthAngle = glm::clamp(cosinusOfSolarAzimuthAngle, -1.0f, 1.0f);
    float solarAzimuthAngleInRadians = acos(cosinusOfSolarAzimuthAngle);
    if (hourAngleInRadians > 0)
        solarAzimuthAngleInRadians = 2 * PI - solarAzimuthAngleInRadians;

    position.x = distance * sin(solarZenithAngleInRadians) * cos(solarAzimuthAngleInRadians);
    position.y = distance * cos(solarZenithAngleInRadians);
    position.z = distance * sin(solarZenithAngleInRadians) * sin(solarAzimuthAngleInRadians);

    // Rendering
    shader.Use();
    shader.SetMat4("view", Camera::camera->GetViewMatrix());
    shader.SetMat4("projection", glm::perspective(glm::radians(Camera::camera->zoom), (float)Application::GetInstance()->windowWidth / (float)Application::GetInstance()->windowHeight, 2000.0f, 5000.0f));
    shader.SetVec3("lightPosition", position);
    shader.SetVec4("color", glm::vec4(1.0, 1.0, 0.0, 1.0));
    shader.SetBool("isSun", true);

    glBindVertexArray(sunVao);
    glDrawArrays(GL_TRIANGLES, 0, sizeof(sunVertices) / sizeof(float) / 3);
    glBindVertexArray(0);
}

void DirectionalLight::CreateSphere() {
    float startU = 0, startV = 0;
    float endU = PI * 2, endV = PI;
    float stepU = (endU - startU) / sectorCount;
    float stepV = (endV - startV) / stackCount;
    for (int i = 0; i < sectorCount; i++) {
        for (int j = 0; j < stackCount; j++) {
            float u = i * stepU + startU;
            float v = j * stepV + startV;
            float un = (i + 1 == sectorCount) ? endU : (i + 1) * stepU + startU;
            float vn = (j + 1 == stackCount) ? endV : (j + 1) * stepV + startV;

            // First triangle
            sunVertices[18 * (i * stackCount + j)] = cosf(u) * sinf(v) * radius;
            sunVertices[18 * (i * stackCount + j) + 1] = cosf(v) * radius;
            sunVertices[18 * (i * stackCount + j) + 2] = sinf(u) * sinf(v) * radius;
            sunVertices[18 * (i * stackCount + j) + 3] = cosf(un) * sinf(v) * radius;
            sunVertices[18 * (i * stackCount + j) + 4] = cosf(v) * radius;
            sunVertices[18 * (i * stackCount + j) + 5] = sinf(un) * sinf(v) * radius;
            sunVertices[18 * (i * stackCount + j) + 6] = cosf(u) * sinf(vn) * radius;
            sunVertices[18 * (i * stackCount + j) + 7] = cosf(vn) * radius;
            sunVertices[18 * (i * stackCount + j) + 8] = sinf(u) * sinf(vn) * radius;
            // Second triangle
            sunVertices[18 * (i * stackCount + j) + 9] = cosf(un) * sinf(vn) * radius;
            sunVertices[18 * (i * stackCount + j) + 10] = cosf(vn) * radius;
            sunVertices[18 * (i * stackCount + j) + 11] = sinf(un) * sinf(vn) * radius;
            sunVertices[18 * (i * stackCount + j) + 12] = cosf(u) * sinf(vn) * radius;
            sunVertices[18 * (i * stackCount + j) + 13] = cosf(vn) * radius;
            sunVertices[18 * (i * stackCount + j) + 14] = sinf(u) * sinf(vn) * radius;
            sunVertices[18 * (i * stackCount + j) + 15] = cosf(un) * sinf(v) * radius;
            sunVertices[18 * (i * stackCount + j) + 16] = cosf(v) * radius;
            sunVertices[18 * (i * stackCount + j) + 17] = sinf(un) * sinf(v) * radius;
        }
    }
}