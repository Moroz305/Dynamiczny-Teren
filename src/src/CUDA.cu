#include "include/CUDA.cuh"
#include "include/CudaNoise.cuh"

 __global__ void CUDACreateHeightmap(unsigned char* output, int size, float frequency, int octaves, unsigned int seed, float mountainRatio, int mountainOctaves) {
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx < size) {
        int textureLength = (int)sqrt((float)size);
        unsigned int x = idx % textureLength;
        unsigned int y = idx / textureLength;

        float fx = static_cast<float>(x) / (float)textureLength * frequency;
        float fy = static_cast<float>(y) / (float)textureLength * frequency;

        float3 pos = make_float3(fx, fy, 0.0f);

        float value = cudaNoise::repeaterPerlin(pos, 1.0f, (int)seed, octaves, 2.0f, 0.5f);

        // Clamp value to [0,1]
        if (value <= -1.0)
            value = 0;
        else if (value >= 1)
            value = 1;
        else
            value = value * 0.5f + 0.5f;

        if (value >= mountainRatio) {
            value = cudaNoise::repeaterPerlin(pos, 1.0f, (int) seed, mountainOctaves, 2.0f, 0.5f);

            if (value <= -1.0)
                value = 0;
            else if (value >= 1)
                value = 1;
            else
                value = value * 0.5f + 0.5f;
        }

        output[idx] = static_cast<unsigned char>(value * 255.0f);
    }
}

void CreateHeightmap(unsigned char* heightmap, unsigned int sizeOfTexture, float frequency, int octaves, unsigned int seed, float mountainRatio, int mountainOctaves) {
    unsigned char* deviceOutput;
    cudaMalloc((void**)&deviceOutput, sizeOfTexture);

    int blockSize = 256;
    int dimSize = (int)(sizeOfTexture + blockSize - 1) / blockSize;
    CUDACreateHeightmap<<<dimSize, blockSize>>>(deviceOutput, (int)sizeOfTexture, frequency, octaves, seed, mountainRatio, mountainOctaves);

    cudaMemcpy(heightmap, deviceOutput, sizeOfTexture, cudaMemcpyDeviceToHost);

    cudaFree(deviceOutput);
}

extern __global__ void CUDACreateIndices(unsigned int* output, unsigned int size, int verticesInRow) {
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx < size) {
        unsigned int x = idx % (verticesInRow - 1);
        unsigned int y = idx / (verticesInRow - 1);

        unsigned int baseIdx = y * verticesInRow + x;

        unsigned int triangle1Idx = idx * 6;
        unsigned int triangle2Idx = triangle1Idx + 3;

        unsigned int vertex1 = baseIdx;
        unsigned int vertex2 = baseIdx + 1;
        unsigned int vertex3 = baseIdx + verticesInRow;
        unsigned int vertex4 = baseIdx + verticesInRow + 1;

        // First triangle
        output[triangle1Idx] = vertex1;
        output[triangle1Idx + 1] = vertex4;
        output[triangle1Idx + 2] = vertex2;

        // Second triangle
        output[triangle2Idx] = vertex1;
        output[triangle2Idx + 1] = vertex3;
        output[triangle2Idx + 2] = vertex4;
    }
}

void CreateIndices(unsigned int* indices, unsigned int sizeOfIndices, int verticesInRow) {
    unsigned int* deviceOutput;
    cudaMalloc((void**)&deviceOutput, sizeOfIndices * sizeof(unsigned int));

    int blockSize = 256;
    int dimSize = (int)(sizeOfIndices / 6 + blockSize - 1) / blockSize;
    CUDACreateIndices<<<dimSize, blockSize>>>(deviceOutput, sizeOfIndices / 6, verticesInRow);

    cudaMemcpy(indices, deviceOutput, sizeOfIndices * sizeof(unsigned int), cudaMemcpyDeviceToHost);

    cudaFree(deviceOutput);
}

extern __global__ void CUDACreateNormalsAndTangents(Vertex* output, unsigned int size, unsigned int verticesInRow) {
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx < size) {
        unsigned int x = idx % (verticesInRow - 1);
        unsigned int y = idx / (verticesInRow - 1);

        unsigned int bsaeIndex = y * verticesInRow + x;

        Vertex& firstVertex = output[bsaeIndex];
        Vertex& secondVertex = output[bsaeIndex + 1];
        Vertex& thirdVertex = output[bsaeIndex + verticesInRow];
        Vertex& fourthVertex = output[bsaeIndex + verticesInRow + 1];

        // Calculate normals
        float3 firstVertexPosition = make_float3(firstVertex.position.x, firstVertex.position.y, firstVertex.position.z);
        float3 secondVertexPosition = make_float3(secondVertex.position.x, secondVertex.position.y, secondVertex.position.z);
        float3 thirdVertexPosition = make_float3(thirdVertex.position.x, thirdVertex.position.y, thirdVertex.position.z);
        float3 fourthVertexPosition = make_float3(fourthVertex.position.x, fourthVertex.position.y, fourthVertex.position.z);

        float3 a = make_float3(fourthVertexPosition.x - firstVertexPosition.x, fourthVertexPosition.y - firstVertexPosition.y, fourthVertexPosition.z - firstVertexPosition.z);
        float3 b = make_float3(secondVertexPosition.x - firstVertexPosition.x, secondVertexPosition.y - firstVertexPosition.y, secondVertexPosition.z - firstVertexPosition.z);

        float3 cross = make_float3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
        float length = sqrt((cross.x * cross.x) + (cross.y * cross.y) + (cross.z * cross.z));

        float3 normal1 = make_float3(cross.x / length, cross.y / length, cross.z / length);

        a = make_float3(thirdVertexPosition.x - firstVertexPosition.x, thirdVertexPosition.y - firstVertexPosition.y, thirdVertexPosition.z - firstVertexPosition.z);
        b = make_float3(fourthVertexPosition.x - firstVertexPosition.x, fourthVertexPosition.y - firstVertexPosition.y, fourthVertexPosition.z - firstVertexPosition.z);

        cross = make_float3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
        length = sqrt((cross.x * cross.x) + (cross.y * cross.y) + (cross.z * cross.z));

        float3 normal2 = make_float3(cross.x / length, cross.y / length, cross.z / length);

        // Add normals for each vertex in quad
        firstVertex.normal.x += normal1.x;
        firstVertex.normal.y += normal1.y;
        firstVertex.normal.z += normal1.z;
        secondVertex.normal.x += normal1.x;
        secondVertex.normal.y += normal1.y;
        secondVertex.normal.z += normal1.z;
        fourthVertex.normal.x += normal1.x;
        fourthVertex.normal.y += normal1.y;
        fourthVertex.normal.z += normal1.z;

        firstVertex.normal.x += normal2.x;
        firstVertex.normal.y += normal2.y;
        firstVertex.normal.z += normal2.z;
        thirdVertex.normal.x += normal2.x;
        thirdVertex.normal.y += normal2.y;
        thirdVertex.normal.z += normal2.z;
        fourthVertex.normal.x += normal2.x;
        fourthVertex.normal.y += normal2.y;
        fourthVertex.normal.z += normal2.z;

        // Calculate tangents
        float2 firstVertexTextureCoords = make_float2(firstVertex.texCoords.x, firstVertex.texCoords.y);
        float2 secondVertexTextureCoords = make_float2(secondVertex.texCoords.x, secondVertex.texCoords.y);
        float2 thirdVertexTextureCoords = make_float2(thirdVertex.texCoords.x, thirdVertex.texCoords.y);
        float2 fourthVertexTextureCoords = make_float2(fourthVertex.texCoords.x, fourthVertex.texCoords.y);

        // First triangle
        float3 edge1 = make_float3(secondVertexPosition.x - firstVertexPosition.x, secondVertexPosition.y - firstVertexPosition.y, secondVertexPosition.z - firstVertexPosition.z);
        float3 edge2 = make_float3(fourthVertexPosition.x - firstVertexPosition.x, fourthVertexPosition.y - firstVertexPosition.y, fourthVertexPosition.z - firstVertexPosition.z);
        float2 deltaUV1 = make_float2(secondVertexTextureCoords.x - firstVertexTextureCoords.x, secondVertexTextureCoords.y - firstVertexTextureCoords.y);
        float2 deltaUV2 = make_float2(fourthVertexTextureCoords.x - firstVertexTextureCoords.x, fourthVertexTextureCoords.y - firstVertexTextureCoords.y);

        float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

        float3 tangent1, tangent2;
        tangent1.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
        tangent1.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
        tangent1.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);

        length = sqrt((tangent1.x * tangent1.x) + (tangent1.y * tangent1.y) + (tangent1.z * tangent1.z));
        tangent1.x /= length;
        tangent1.y /= length;
        tangent1.z /= length;

        // Second triangle
        edge1 = make_float3(thirdVertexPosition.x - firstVertexPosition.x, thirdVertexPosition.y - firstVertexPosition.y, thirdVertexPosition.z - firstVertexPosition.z);
        edge2 = make_float3(fourthVertexPosition.x - firstVertexPosition.x, fourthVertexPosition.y - firstVertexPosition.y, fourthVertexPosition.z - firstVertexPosition.z);
        deltaUV1 = make_float2(thirdVertexTextureCoords.x - firstVertexTextureCoords.x, thirdVertexTextureCoords.y - firstVertexTextureCoords.y);
        deltaUV2 = make_float2(fourthVertexTextureCoords.x - firstVertexTextureCoords.x, fourthVertexTextureCoords.y - firstVertexTextureCoords.y);

        f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

        tangent2.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
        tangent2.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
        tangent2.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);

        length = sqrt((tangent2.x * tangent2.x) + (tangent2.y * tangent2.y) + (tangent2.z * tangent2.z));
        tangent2.x /= length;
        tangent2.y /= length;
        tangent2.z /= length;

        // Add tangents for each vertex in quad
        firstVertex.tangent.x += tangent1.x;
        firstVertex.tangent.y += tangent1.y;
        firstVertex.tangent.z += tangent1.z;
        secondVertex.tangent.x += tangent1.x;
        secondVertex.tangent.y += tangent1.y;
        secondVertex.tangent.z += tangent1.z;
        fourthVertex.tangent.x += tangent1.x;
        fourthVertex.tangent.y += tangent1.y;
        fourthVertex.tangent.z += tangent1.z;

        firstVertex.tangent.x += tangent2.x;
        firstVertex.tangent.y += tangent2.y;
        firstVertex.tangent.z += tangent2.z;
        thirdVertex.tangent.x += tangent2.x;
        thirdVertex.tangent.y += tangent2.y;
        thirdVertex.tangent.z += tangent2.z;
        fourthVertex.tangent.x += tangent2.x;
        fourthVertex.tangent.y += tangent2.y;
        fourthVertex.tangent.z += tangent2.z;
    }
}

void CreateNormalsAndTangents(Vertex* vertices, unsigned int sizeOfVertices, unsigned int verticesInRow) {
    Vertex* deviceOutput;
    cudaMalloc((void**)&deviceOutput, sizeOfVertices * sizeof(Vertex));

    cudaMemcpy(deviceOutput, vertices, sizeOfVertices * sizeof(Vertex), cudaMemcpyHostToDevice);

    int blockSize = 256;
    int dimSize = (int)(sizeOfVertices + blockSize - 1) / blockSize;
    CUDACreateNormalsAndTangents<<<dimSize, blockSize>>>(deviceOutput, sizeOfVertices, verticesInRow);

    cudaMemcpy(vertices, deviceOutput, sizeOfVertices * sizeof(Vertex), cudaMemcpyDeviceToHost);

    cudaFree(deviceOutput);
}

extern __global__ void CUDACreateForestMap(unsigned char* output, const unsigned char* forestMap, unsigned int size, int forestMapLength, int activeNeighboursRequired) {
    unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx < size) {
        unsigned int x = idx % forestMapLength;
        unsigned int y = idx / forestMapLength;

        int activeNeighboursCount = 0;

        if (forestMap[idx])
            activeNeighboursCount++;

        if (x > 0) {
            if (forestMap[idx - 1])
                activeNeighboursCount++;
            if (y > 0  &&  forestMap[idx - forestMapLength - 1])
                activeNeighboursCount++;
        }
        if (y > 0) {
            if (forestMap[idx - forestMapLength])
                activeNeighboursCount++;
            if (x < forestMapLength - 1  &&  forestMap[idx - forestMapLength + 1])
                activeNeighboursCount++;
        }
        if (x < forestMapLength - 1) {
            if (forestMap[idx + 1])
                activeNeighboursCount++;
            if (y < forestMapLength - 1  &&  forestMap[idx + forestMapLength + 1])
                activeNeighboursCount++;
        }
        if (y < forestMapLength - 1) {
            if (forestMap[idx + forestMapLength])
                activeNeighboursCount++;
            if (x > 0  &&  forestMap[idx + forestMapLength - 1])
                activeNeighboursCount++;
        }

        if (activeNeighboursCount > activeNeighboursRequired)
            output[idx] = 255;
        else
            output[idx] = 0;
    }
}

void CreateForestMap(unsigned char* forestMap, unsigned int sizeOfTexture, int forestMapLength, int iterations, int activeNeighboursRequired[5]) {
    unsigned char* deviceOutput;
    unsigned char* temporaryForestMap;

    cudaMalloc((void**)&deviceOutput, sizeOfTexture);
    cudaMalloc((void**)&temporaryForestMap, sizeOfTexture);

    cudaMemcpy(temporaryForestMap, forestMap, sizeOfTexture, cudaMemcpyHostToDevice);

    int blockSize = 256;
    int dimSize = (int)(sizeOfTexture + blockSize - 1) / blockSize;
    for (int i = 0; i < iterations; ++i) {
        CUDACreateForestMap<<<dimSize, blockSize>>>(deviceOutput, temporaryForestMap, sizeOfTexture, forestMapLength, activeNeighboursRequired[i]);
        cudaMemcpy(temporaryForestMap, deviceOutput, sizeOfTexture, cudaMemcpyDeviceToDevice);
    }

    cudaMemcpy(forestMap, temporaryForestMap, sizeOfTexture, cudaMemcpyDeviceToHost);

    cudaFree(deviceOutput);
    cudaFree(temporaryForestMap);
}