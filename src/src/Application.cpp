#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <spdlog/spdlog.h>
#include "stb_image.h"
#include "imgui.h"
#include "imgui_impl/imgui_impl_glfw.h"
#include "imgui_impl/imgui_impl_opengl3.h"
#include "include/Application.h"
#include "include/Terrain.h"
#include "include/Camera.h"
#include "include/DirectionalLight.h"
#include "include/Skybox.h"
#include "include/Model.h"

#define PI 3.14159265358979323846

Application::Application() = default;

Application::~Application() = default;

Application *Application::GetInstance() {
    if (application == nullptr) {
        application = new Application();
    }
    return application;
}

int Application::Start() {
    // Setup window
    glfwSetErrorCallback(GlfwErrorCallback);
    if (!glfwInit())
        return 1;

    // GL 4.3 + GLSL 430
    const char* glsl_version = "#version 430";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_SAMPLES, 4);

    // Create window with graphics context
    window = glfwCreateWindow(1280, 720, "Dynamiczny Teren", nullptr, nullptr);
    if (window == nullptr)
        return 1;
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);
    glfwSetCursorPosCallback(window, MouseCallback);
    glfwSetScrollCallback(window, ScrollCallback);
    glfwSwapInterval(1);

    // Center window on the screen
    const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    int monitorWidth = mode->width;
    int monitorHeight = mode->height;
    glfwSetWindowPos(window, monitorWidth / 2 - windowWidth / 2, monitorHeight / 2 - windowHeight / 2);

    // Initialize OpenGL loader
    bool err = !gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
    if (err)
    {
        spdlog::error("Failed to initialize OpenGL loader!");
        return 1;
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_CULL_FACE);
    glEnable(GL_MULTISAMPLE);

    // tell stb_image.h to flip loaded texture's on the y-axis (before loading model).
    stbi_set_flip_vertically_on_load(true);

    // Setup Dear ImGui binding
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    ImVector<ImWchar> ranges;
    ImFontGlyphRangesBuilder builder;
    builder.AddRanges(io.Fonts->GetGlyphRangesDefault());
    builder.AddText("ąćęłńóśźżĄĆĘŁŃÓŚŹŻ");
    builder.BuildRanges(&ranges);
    std::string filename = fontPath;
    filename += "Ubuntu.ttf";
    io.Fonts->AddFontFromFileTTF(filename.c_str(), 16.0f, nullptr, ranges.Data);
    io.Fonts->Build();

    return 0;
}

void Application::Update() {
    // Create variables
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
    Camera* camera = Camera::GetInstance();
    camera->position = glm::vec3(0, 300, 300);
    DirectionalLight* directionalLight = DirectionalLight::GetInstance();
    Skybox skybox(Shader("skybox.vert", "skybox.frag"));

    basicShader = new Shader("basic.vert", "basic.frag");
    terrainShader = new Shader("terrain.vert", "terrain.frag");
    grassShader = new Shader("grass.vert", "grass.frag");
    waterShader = new Shader("water.vert", "water.frag");
    auto* depthShader = new Shader("depth.vert", "depth.frag");
    std::vector<Shader*> shaders{basicShader, terrainShader, grassShader, waterShader};

    Terrain* terrain = Terrain::GetInstance();

    leaves = new Model("leaves.obj", 0, true);
    trunk = new Model("trunk.obj", 0);
    grass = new Model("grass.obj", 0);
    flowers = new Model("flowers.obj", 0);

    bool debugWindow = false;

    // Configure depth map FBO
    const unsigned int shadowWidth = 4096, shadowHeight = 4096;
    unsigned int depthMapFBO;
    {
        glGenFramebuffers(1, &depthMapFBO);
        // Create depth texture
        glGenTextures(1, &depthMap);
        glBindTexture(GL_TEXTURE_2D, depthMap);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadowWidth, shadowHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT,nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        float borderColor[] = {1.0, 1.0, 1.0, 1.0};
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
        // Attach depth texture as FBO's depth buffer
        glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        for (const auto & shader : shaders) {
            shader->Use();
            shader->SetInt("shadowMap", 0);
        }
    }

    basicShader->Use();
    basicShader->SetInt("noiseTexture", 1);


    while (!glfwWindowShouldClose(window)) {
        auto currentFrame = (float)glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        ProcessInput();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // Create ImGui windows
        {
            {
                ImGui::Begin("Okno");
                if (ImGui::Button("Debug")) {
                    debugWindow = !debugWindow;
                }
                ImGui::SameLine(ImGui::GetWindowWidth() - 75);
                ImGui::Text("%.1f FPS", ImGui::GetIO().Framerate);
                if (ImGui::BeginTabBar("Ustawienia", ImGuiTabBarFlags_None)) {
                    if (ImGui::BeginTabItem("Teren")) {
                        ImGui::PushItemWidth(405);
                        ImGui::SliderInt("Godzina", &hour, 0, 23);
                        ImGui::SliderInt("Dzień", &day, 1, 365);
                        ImGui::Text("");
                        if (ImGui::Button("Stwórz teren")) {
                            terrain->CreateTerrain();
                        }
                        ImGui::EndTabItem();
                    }
                    if (ImGui::BeginTabItem("Mapa wysokości")) {
                        ImGui::PushItemWidth(380);
                        ImGui::SliderFloat("Częstotliwość", &terrain->frequency, 0.1, 64);
                        ImGui::SliderInt("Liczba oktaw", &terrain->octaves, 1, 16);
                        ImGui::SliderInt("Numer ziarna", (int*)(&terrain->seed), 0, 1000000000);
                        ImGui::EndTabItem();
                    }
                    if (ImGui::BeginTabItem("Mapa lasu")) {
                        ImGui::PushItemWidth(200);
                        ImGui::SliderFloat("Prawdopodobieństwo aktywnej komórki", &terrain->probability, 0.0, 1.0);
                        ImGui::SliderInt("Liczba iteracji", &terrain->iterations, 0, 5);
                        ImGui::Text("Liczba sąsiadów do aktywowania komórki");
                        ImGui::PushItemWidth(500);
                        int min = 0, max = 8;
                        ImGui::DragScalarN("",ImGuiDataType_S32,terrain->activeNeighboursRequired,terrain->iterations,0.2,&min,&max,"%d",ImGuiSliderFlags_AlwaysClamp);
                        ImGui::EndTabItem();
                    }
                    ImGui::EndTabBar();
                }
                ImGui::End();
            }

            if (debugWindow) {
                ImGui::Begin("Debug", &debugWindow);
                ImGui::Checkbox("Wyświetlanie krawędzi terenu", &renderTerrainEdges);
                ImGui::Checkbox("Wyświetlanie kierunku światła", &renderLightVectors);
                ImGui::End();
            }

            if (terrainCreationWindow) {
                ImGui::Begin("Teren", &terrainCreationWindow);
                std::string verticesInRow = "Liczba wierzchołków w rzędzie: " + std::to_string(terrain->currentTerrainVerticesInRow);
                ImGui::Text("%s", verticesInRow.c_str());
                std::string length = "Długość: " + std::to_string(terrain->currentTerrainLength) + " m";
                ImGui::Text("%s", length.c_str());
                std::string textureRepetitionsInRow = "Powtórzenia tekstury w rzędzie: " + std::to_string(terrain->currentTerrainTextureRepetitionsInRow);
                ImGui::Text("%s", textureRepetitionsInRow.c_str());
                ImGui::Text("");
                std::string frequency = "Częstotliwość: " + std::format("{:.3f}", terrain->currentTerrainFrequency) + " Hz";
                ImGui::Text("%s", frequency.c_str());
                std::string octaves = "Liczba oktaw: " + std::to_string(terrain->currentTerrainOctaves);
                ImGui::Text("%s", octaves.c_str());
                std::string seed = "Numer ziarna: " + std::to_string(terrain->currentTerrainSeed);
                ImGui::Text("%s", seed.c_str());
                std::string creationTime = "Czas tworzenia: " + std::to_string(terrain->creationTime) + " s";
                ImGui::Text("%s", creationTime.c_str());
                ImGui::SameLine();
                float fpsCounter = 1.0f / terrain->creationTime;
                ImVec4 textColor(0, 1, 0, 1);
                if (fpsCounter < 60)    textColor.x = 1;
                if (fpsCounter < 60)    textColor.y = 0.5;
                if (fpsCounter < 30)    textColor.y = 0;
                std::string fpsCounterString = "(" + std::format("{:.1f}", fpsCounter) + "  FPS)";
                ImGui::SetCursorPosX(ImGui::GetCursorPosX() + ImGui::GetColumnWidth() - ImGui::CalcTextSize(fpsCounterString.c_str()).x - ImGui::GetScrollX());
                ImGui::TextColored(textColor, "%s", fpsCounterString.c_str());
                ImGui::Text("");
                if (ImGui::BeginTabBar("Tekstury", ImGuiTabBarFlags_None)) {
                    if (ImGui::BeginTabItem("Mapa wysokości")) {
                        ImGui::Image((ImTextureID)(unsigned long long)terrain->heightMapID, ImVec2(300, 300));
                        ImGui::EndTabItem();
                    }
                    if (ImGui::BeginTabItem("Mapa lasu")) {
                        ImGui::Image((ImTextureID)(unsigned long long)terrain->forestMapID, ImVec2(300, 300));
                        ImGui::EndTabItem();
                    }
                    ImGui::EndTabBar();
                }
                ImGui::End();
            }
        }

        // Rendering
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        directionalLight->direction = glm::normalize(-directionalLight->position);

        // Render depth of scene to texture (from light's perspective)
        glm::mat4 lightProjection, lightView;

        lightProjection = glm::ortho(-600.0f, 600.0f, -600.0f, 600.0f, 0.0f, 300.0f);

        if (camera->position.x < -120)
            lightView = glm::lookAt(glm::vec3(0.1) * -directionalLight->direction, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
        else
            lightView = glm::lookAt(glm::vec3(1.14f * camera->position.x + 140) * -directionalLight->direction, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));

        lightSpaceMatrix = lightProjection * lightView;
        // Render scene from light's point of view
        glViewport(0, 0, shadowWidth, shadowHeight);
        glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
        glClear(GL_DEPTH_BUFFER_BIT);

        // Render scene to depth map
        {
            depthShader->Use();
            depthShader->SetMat4("lightSpaceMatrix", lightSpaceMatrix);
            terrain->Draw(depthShader);

            leaves->Draw(depthShader);
            trunk->Draw(depthShader);
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        // Reset viewport
        glViewport(0, 0, windowWidth, windowHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        for (const auto & shader : shaders) {
            shader->Use();
            shader->SetMat4("projection", glm::perspective(glm::radians(Camera::camera->zoom), (float)windowWidth / (float)windowHeight, 0.1f, 1000.0f));
            shader->SetMat4("view", Camera::camera->GetViewMatrix());
            shader->SetVec3("lightPos", directionalLight->position);
            shader->SetVec3("viewPos", Camera::camera->position);
            shader->SetMat4("lightSpaceMatrix", lightSpaceMatrix);
            shader->SetVec3("directionalLight.position", directionalLight->position);
            shader->SetVec3("directionalLight.ambient", directionalLight->ambient);
            shader->SetVec3("directionalLight.diffuse", directionalLight->diffuse);
            shader->SetVec3("directionalLight.specular", directionalLight->specular);
            shader->SetInt("dayOfTheYear", day);
        }

        // Draw objects
        {
            terrain->Draw(terrainShader);

            terrain->DrawLakes();

            basicShader->Use();
            leaves->Draw();
            trunk->Draw();
            flowers->Draw();

            glDisable(GL_CULL_FACE);
            terrain->DrawGrass();
            glEnable(GL_CULL_FACE);

            if (renderLightVectors)
                directionalLight->DrawLines();

            directionalLight->DrawSun();

            // Draw skybox as last
            skybox.Draw();
        }

        // Draw ImGui windows
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // Delete pointers
    delete camera;
    delete directionalLight;
    delete terrain;
    glDeleteProgram(basicShader->id);
    delete basicShader;
    glDeleteProgram(terrainShader->id);
    delete terrainShader;
    glDeleteProgram(depthShader->id);
    delete depthShader;
    delete trunk;
    delete leaves;
    delete grass;
    delete flowers;
}

void Application::End() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();
}

unsigned int Application::LoadTexture(const char *filename, bool model) {
    std::string path;
    if (model) {
        path += modelPath;
        path += std::string(filename).substr(0, std::string(filename).find_first_of('_'));
        path += '/' + std::string(filename);
    } else {
        path += texturePath;
        path += filename;
    }

    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width, height, nrComponents;
    unsigned char *data = stbi_load(path.c_str(), &width, &height, &nrComponents, 0);
    if (data) {
        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, (GLint)format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else {
        std::cout << "Texture failed to load at path: " << path << std::endl;
    }
    stbi_image_free(data);

    return textureID;
}

void Application::GlfwErrorCallback(int error, const char *description) {
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

void Application::FramebufferSizeCallback([[maybe_unused]] GLFWwindow *window, int width, int height) {
    glViewport(0, 0, width, height);
}

void Application::MouseCallback([[maybe_unused]] GLFWwindow *window, double xposIn, double yposIn) {
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_RELEASE)
        application->firstMouse = true;
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) != GLFW_PRESS) {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        return;
    }
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    auto xpos = static_cast<float>(xposIn);
    auto ypos = static_cast<float>(yposIn);

    if (application->firstMouse)
    {
        application->lastX = xpos;
        application->lastY = ypos;
        application->firstMouse = false;
    }

    float xoffset = xpos - application->lastX;
    float yoffset = application->lastY - ypos;

    application->lastX = xpos;
    application->lastY = ypos;

    Camera::camera->ProcessMouseMovement(xoffset, yoffset);
}

void Application::ScrollCallback([[maybe_unused]] GLFWwindow *window, [[maybe_unused]] double xoffset, double yoffset) {
    Camera::camera->ProcessMouseScroll((float)yoffset);
}

void Application::ProcessInput() {
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        Camera::camera->ProcessKeyboard(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        Camera::camera->ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        Camera::camera->ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        Camera::camera->ProcessKeyboard(RIGHT, deltaTime);

    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
        Camera::camera->ProcessZoom(1);
    else
        Camera::camera->ProcessZoom(-1);
}