#version 430 core
out vec4 FragColor;

struct DirectionalLight {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoord;
in vec4 FragPosLightSpace;
in vec3 TangentLightPos;
in vec3 TangentViewPos;
in vec3 TangentFragPos;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform sampler2D texture_normal1;
uniform sampler2D texture_height1;
uniform sampler2D shadowMap;
uniform sampler2D noiseTexture;

uniform bool hasNormalTexture;
uniform bool isItVegetation;
uniform vec3 viewPos;
uniform int dayOfTheYear;
uniform DirectionalLight directionalLight;

float ShadowCalculation(vec4 fragPosLightSpace, vec3 normal)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, projCoords.xy).r;
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // calculate bias (based on depth map resolution and slope)
    vec3 lightDir;
    if (hasNormalTexture)
        lightDir = normalize(TangentLightPos - TangentFragPos);
    else
        lightDir = normalize(directionalLight.position - FragPos);
    float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);
    // PCF
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;

    // keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
    if(projCoords.z > 1.0)
        shadow = 0.0;

    return shadow;
}

void main()
{
    vec4 textureDiffuse;
    if (isItVegetation  &&  dayOfTheYear > 266  &&  dayOfTheYear < 304) {
        float noise = texture(noiseTexture, TexCoord * 2.5).r;
        textureDiffuse = noise * vec4(1.0, 0.63, 0.0, 1.0) + (1 - noise) * vec4(1.0, 0.0, 0.0, 1.0);
    } else {
        textureDiffuse = texture(texture_diffuse1, TexCoord);
    }

    if(textureDiffuse.a < 0.1)
        discard;

    vec3 normal = normalize(Normal);

    if (hasNormalTexture) {
        // obtain normal from normal map in range [0,1]
        normal = texture(texture_normal1, TexCoord).rgb;
        // transform normal vector to range [-1,1]
        normal = normalize(normal * 2.0 - 1.0);// this normal is in tangent space
    }

    vec3 color = textureDiffuse.rgb;
    float shininess = 8;

    // ambient
    vec3 ambient = directionalLight.ambient;

    // diffuse
    vec3 lightDir;
    if (hasNormalTexture)
        lightDir = normalize(TangentLightPos - TangentFragPos);
    else
        lightDir = normalize(directionalLight.position - FragPos);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = directionalLight.diffuse * diff;

    // specular
    vec3 viewDir;
    if (hasNormalTexture)
        viewDir = normalize(TangentViewPos - TangentFragPos);
    else
        viewDir = normalize(viewPos - FragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), shininess);
    vec3 specular = directionalLight.specular * spec;

    // calculate shadow
    float shadow = ShadowCalculation(FragPosLightSpace, normal);
    FragColor = vec4((ambient + (1.0 - shadow) * (diffuse + specular)) * color, 1.0);
}