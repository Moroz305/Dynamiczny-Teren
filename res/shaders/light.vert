#version 430 core
layout (location = 0) in vec3 position;

uniform mat4 view;
uniform mat4 projection;

uniform vec3 lightPosition;
uniform bool isSun;

void main()
{
    if (isSun) {
        gl_Position = projection * view * vec4(position + lightPosition, 1.0);
    } else {
        if (position.y == 1.0f)
            gl_Position = projection * view * vec4(lightPosition, 1.0);
        else
            gl_Position = projection * view * vec4(position, 1.0);
    }
}