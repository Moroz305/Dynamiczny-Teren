#version 430 core
out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D grass_texture;
uniform sampler2D grass_alpha_texture;

void main() {
    FragColor = vec4(texture(grass_texture, TexCoord).rgb * vec3(0.0, 0.75, 0.0), texture(grass_alpha_texture, TexCoord).r);
}