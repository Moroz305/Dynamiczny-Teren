#version 430 core
layout (location = 0) in vec3 position;
layout (location = 5) in mat4 instanceMatrix;

uniform mat4 lightSpaceMatrix;

void main()
{
    gl_Position = lightSpaceMatrix * instanceMatrix * vec4(position, 1.0);
}