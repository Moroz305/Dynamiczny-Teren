#version 430 core
out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D water_texture_diffuse;

void main() {
    FragColor = vec4(texture(water_texture_diffuse, TexCoord).rgb, 1.0f);
}